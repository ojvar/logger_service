"use strict";

const mix = require("laravel-mix");
require("laravel-mix-alias");

mix.setPublicPath("public");
if (process.env.NODE_ENV === "development") {
    mix.sourceMaps();
}
mix.version();

/* Aliases */
mix.alias({
    "@PUBLIC": "public",
    "@CERTS": "src/resources/certs",
    "@JS": "src/resources/js",
    "@SASS": "src/resources/sass",
    "@VIEWS": "src/resources/views",
    "@IMAGES": "src/resources/images",
});

mix.js("src/resources/js/core.js", "public/js/").js(
    "src/resources/js/monitor.js",
    "public/js/"
);

mix.sass("src/resources/sass/core.scss", "public/css/");
