"use strict";

const Path = require("path");
const DotEnv = require("dotenv");

/* Load .env data */
DotEnv.config();

/* Load global modules */
const GlobalHelper = require(Path.resolve("src/helpers/global-helper"));
GlobalHelper.init();

/* Boot app */
const App = use("src/app");
App.boot();
