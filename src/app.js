"use strict";

/**
 * App function
 */
function App() {}
module.exports = App;

/**
 * Initialize app
 */
App.boot = function boot() {
    /* Create app instance */
    global.app = App.initExpress();
};

/**
 * Init Express
 */
App.initExpress = async function initExpress() {
    /* Setup express */
    const ExpressHelper = use("src/helpers/express-helper");
    let app = await ExpressHelper.init();

    /* Setup Mongoose */
    const MongooseHelper = use("src/helpers/mongoose-helper");
    await MongooseHelper.init();

    /* Setup eventHander */
    const EventHelper = use("src/helpers/event-helper");
    await EventHelper.init();

    /* Start Services */
    const ServiceHelper = use("src/helpers/service-helper");
    await ServiceHelper.init();

    return app;
};
