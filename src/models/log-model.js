"use strict";

const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

/**
 * Log Model
 */
function Model() {}
module.exports = Model;

/**
 * Init function
 */
Model.init = function init() {
    const schema = Model.getSchema();

    Mongoose.model("Log", schema);
};

/**
 * Generate schema
 */
Model.getSchema = function getSchema() {
    const Model = new Schema(
        {
            type: {
                type: String,
                required: true,
                trim: true,
            },
            tag: {
                type: String,
                required: true,
                trim: true,
            },
            log: {
                type: String,
                required: true,
                trim: true,
            },
        },
        { timestamps: { createdAt: "created_at" } }
    );

    return Model;
};
