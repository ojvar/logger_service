"use strict";

/**
 * Log request validator
 */
function Validator() {}
module.exports = Validator;

/**
 *
 * @param {Request} req The request
 * @param {Response} res The response
 * @param {Function} next Next function callback
 */
Validator.validate = async function validate(req, res, next) {
    const data = Validator.grabData(req);
    const isValid = Validator.validateData(data);

    if (isValid) {
        next();
    } else {
        res.status(400)
            .send("Invalid data")
            .end();
    }
};

/**
 * Grab data from request
 * @param {Request} req The request object
 */
Validator.grabData = function grabData(req) {
    return {
        tag: req.body.tag,
        log: req.body.log,
        // type: req.body.type,
    };
};

/**
 * Validate data object
 * @param {Object} data Data object
 */
Validator.validateData = function validateData(data) {
    return data.tag != null && data.log != null;
};
