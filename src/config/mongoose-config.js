"use strict";

const url = process.env.DB_URL || "mongodb://localhost/logger_db";

module.exports = {
    url,
    options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    },
};
