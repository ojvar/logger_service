"use strict";

const host = process.env.HOST || "127.0.0.1";
const port = process.env.PORT || 8585;
const serverUrl = process.env.URL || `${host}:${port}`;

module.exports = {
    port,
    host,
    serverUrl,
};
