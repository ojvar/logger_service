"use strict";

/* Setup routes */
function LoggerRouter() {}
module.exports = LoggerRouter;

/**
 * Logger router
 * @param {Application} App App instance object
 * @param {Express} Express Express object
 */
LoggerRouter.init = function init(App, Express) {
    const router = Express.Router();

    LoggerRouter.setupRoutes(router);

    return {
        prefix: "",
        router,
    };
};

/**
 * Setup routes
 * @param {Router} router The router object
 */
LoggerRouter.setupRoutes = function setupRoutes(router) {
    const LoggerController = use("src/controllers/logger-controller");
    const LogRequestValidator = use(
        "src/request-validators/log-request-validator"
    );

    router.post("/info", LogRequestValidator.validate, LoggerController.info);
    router.post(
        "/warning",
        LogRequestValidator.validate,
        LoggerController.warning
    );
    router.post("/error", LogRequestValidator.validate, LoggerController.error);
    router.post(
        "/verbose",
        LogRequestValidator.validate,
        LoggerController.verbose
    );
};
