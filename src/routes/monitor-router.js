"use strict";

/* Setup routes */
function LoggerRouter() {}
module.exports = LoggerRouter;

/**
 * Logger router
 * @param {Application} App App instance object
 * @param {Express} Express Express object
 */
LoggerRouter.init = function init(App, Express) {
    const router = Express.Router();

    LoggerRouter.setupRoutes(router);

    return {
        prefix: "/monitor",
        router,
    };
};

/**
 * Setup routes
 * @param {Router} router The router object
 */
LoggerRouter.setupRoutes = function setupRoutes(router) {
    const MonitorController = use("src/controllers/monitor-controller");

    router.get("/", MonitorController.index);
};
