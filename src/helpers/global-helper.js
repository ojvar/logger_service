"use strict";

const _ = require("lodash");
const Path = require("path");

/**
 * Export global data
 */
function GlobalHelper() {}
module.exports = GlobalHelper;

/**
 * Init function
 */
GlobalHelper.init = function init() {
    global.jsLog = GlobalHelper.jsLog;
    global.rPath = GlobalHelper.rPath;
    global.use = GlobalHelper.use;
    global.config = GlobalHelper.config;
};

/**
 * Print json strigified data
 */
GlobalHelper.jsLog = function jsLog(data) {
    console.log("\n");
    console.log(JSON.stringify(data));
    console.log("\n");
};

/**
 * Return resolved path
 */
GlobalHelper.rPath = function rPath() {
    return Path.resolve(...arguments);
};

/**
 * Require a module
 */
GlobalHelper.use = function use() {
    return require(GlobalHelper.rPath(...arguments));
};

/**
 * Load a config file
 * @param {String} name Config filename
 * @param {String} key Key name
 */
GlobalHelper.config = function config(name, key) {
    /* Add -config postfix to the name */
    if (!name.endsWith("-config")) {
        name += "-config";
    }

    const configData = GlobalHelper.use(`src/config/${name}`);
    let result = null;

    if (key) {
        result = _.get(configData, key);
    } else {
        result = configData;
    }

    return result;
};
