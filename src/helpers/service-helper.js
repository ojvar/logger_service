"use strict";

const Glob = require("glob");

/**
 * Event Helper
 */
function ServiceHelper() {}
module.exports = ServiceHelper;

/**
 * Load handler
 */
ServiceHelper.init = async function init() {
    await ServiceHelper.loadServices();
};

/**
 * Load services
 */
ServiceHelper.loadServices = function loadServices() {
    return new Promise((resolve, reject) => {
        const routersDir = rPath("src/services", "**/*.js");

        /* Prepare */
        ServiceHelper.services = {};

        /* Load services */
        Glob(routersDir, async (err, files) => {
            if (err) {
                console.error(err);
                process.exit(1);
                reject(err);
            } else {
                for (let i = 0; i < files.length; ++i) {
                    const file = files[i];
                    const service = use(file);

                    await service.init();
                }

                resolve();
            }
        });
    });
};

/**
 * Raise an event
 * @param {String} name Event name
 * @param {Object} payload Payload data
 */
ServiceHelper.event = function event(name, payload) {
    const handler = ServiceHelper.handlers[name];

    if (handler) {
        handler.handle(payload);
    }
};
