"use strict";

const FS = require("fs");
const Http = require("http");
const Https = require("https");
const Express = require("express");
const Glob = require("glob");
const BodyParser = require("body-parser");

/**
 * Express helper class
 */
function ExpressHelper() {}
module.exports = ExpressHelper;

/**
 * Init express
 */
ExpressHelper.init = async function init() {
    /* Setup express */
    const App = Express();

    /* Setup routes */
    ExpressHelper.loadRoutes(App);

    /* Start listening */
    ExpressHelper.start(App);

    return App;
};

/**
 * Start app
 * @param {Application} app Express application instance
 */
ExpressHelper.start = function start(app) {
    /* Read config */
    let serverConfig = config("server");

    /* Setup pug data */
    app.set("view engine", "pug");
    app.use(Express.static(rPath("public")));
    app.set("views", rPath("src/resources/views"));

    // parse application/x-www-form-urlencoded
    app.use(BodyParser.urlencoded({ extended: false }));
    app.use(BodyParser.json());

    /* Setup https version */
    const privateKey = FS.readFileSync(
        rPath("src/certificates/site_server.key"),
        "utf8"
    );
    const certificate = FS.readFileSync(
        rPath("src/certificates/site_server.cert"),
        "utf8"
    );

    const credentials = { key: privateKey, cert: certificate };
    const httpsServer = Https.createServer(credentials, app);
    httpsServer.listen(serverConfig.port, serverConfig.host);

    /* Start listening */
    console.info(
        `
Server started at
    https://${serverConfig.host}:${serverConfig.port}
        {
            Host: ${serverConfig.host}
            Port: ${serverConfig.port}
            Url: ${serverConfig.serverUrl}
        }
`
    );
};

/**
 * Load routes
 * @param {Application} app Express application instance
 */
ExpressHelper.loadRoutes = function loadRoutes(app) {
    const routersDir = rPath("src/routes", "**/*.js");

    Glob(routersDir, (err, files) => {
        if (err) {
            console.error(err);
            process.exit(1);
        } else {
            for (let i = 0; i < files.length; ++i) {
                const file = files[i];
                let routerData = use(file);
                routerData = routerData.init(app, Express);

                app.use(`${routerData.prefix}`, routerData.router);
            }

            /* Add error and not-found routes */
            app.use((req, res, next) => {
                res.status(404);

                // respond with html page
                if (req.accepts("html")) {
                    res.status(404)
                        .send(`Url Not found ${req.url}`)
                        .end();
                }
                // respond with json
                else if (req.accepts("json")) {
                    res.send({ error: "Not found" });
                } else {
                    // default to plain-text. send()
                    res.type("txt").send("Not found");
                }
            });

            app.use((err, req, res, next) => {
                console.error(err);
                res.status(500)
                    .send(err)
                    .end();
            });
        }
    });
};
