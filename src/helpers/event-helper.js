"use strict";

const Glob = require("glob");

/**
 * Event Helper
 */
function EventHelper() {}
module.exports = EventHelper;

/**
 * Load handler
 */
EventHelper.init = async function init() {
    await EventHelper.loadHandlers();

    /* Add to global data */
    global.events = EventHelper;
};

/**
 * Load handler
 */
EventHelper.loadHandlers = function loadHandlers() {
    return new Promise((resolve, reject) => {
        const routersDir = rPath("src/handlers", "**/*.js");

        /* Prepare */
        EventHelper.handlers = {};

        /* Load handlers */
        Glob(routersDir, async (err, files) => {
            if (err) {
                console.error(err);
                process.exit(1);
                reject(err);
            } else {
                for (let i = 0; i < files.length; ++i) {
                    const file = files[i];
                    const handler = use(file);

                    EventHelper.handlers[handler.name] = handler.handler;
                }

                resolve();
            }
        });
    });
};

/**
 * Raise an event
 * @param {String} name Event name
 * @param {Object} payload Payload data
 */
EventHelper.event = function event(name, payload) {
    const handler = EventHelper.handlers[name];

    if (handler) {
        handler.handle(payload);
    }
};
