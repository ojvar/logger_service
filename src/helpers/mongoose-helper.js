"use strict";

const Mongoose = require("mongoose");
const Glob = require("glob");

/**
 * Mongoose Helper
 */
function MongooseHelper() {}
module.exports = MongooseHelper;

/**
 * Init method
 */
MongooseHelper.init = async function init() {
    /* Load config */
    const mongooseConfig = config("mongoose");

    /* Try to connect */
    const db = await Mongoose.connect(
        mongooseConfig.url,
        mongooseConfig.options
    );

    await MongooseHelper.loadModels();

    /* Setup global data */
    global.db = db;
};

/**
 * Load-models method
 */
MongooseHelper.loadModels = function loadModels() {
    return new Promise((resolve, reject) => {
        const routersDir = rPath("src/models", "**/*.js");

        Glob(routersDir, async (err, files) => {
            if (err) {
                console.error(err);
                process.exit(1);
                reject(err);
            } else {
                for (let i = 0; i < files.length; ++i) {
                    const file = files[i];
                    const model = use(file);

                    await model.init();
                }

                resolve();
            }
        });
    });
};
