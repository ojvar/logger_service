"use strict";

import IO from "socket.io-client";

/* Monitor page */
function Monitor() {}
export default Monitor;

/**
 * Init method
 */
Monitor.init = function init() {
    new Vue({
        data: {
            filter: null,
            url: "",
            logs: [],
            isConnected: false,
        },

        computed: {
            hasLogs: (state) => (state.logs || []).length,
        },

        /**
         * Reset filter data
         */
        created() {
            Vue.set(this, "url", document.getElementById("wsServer").value);

            this.resetFilter();
            this.tryToConnect();
        },

        methods: {
            /**
             * Clear logs
             */
            clearLogs() {
                this.logs.splice(0, this.logs.length);
            },

            /**
             * Try to connect ws-server
             */
            async tryToConnect() {
                const io = IO(this.url);

                io.on("connect", () => {
                    this.setConnectionStatus(true);
                });

                io.on("message", (data) => {
                    data = JSON.parse(data);

                    /* Filter data */
                    if (data.tag.indexOf(this.filter.tag) > -1) {
                        this.appendLog(data);
                    }
                });

                io.on("disconnect", () => {
                    this.setConnectionStatus(false);
                    alert("Connection closed");
                });

                window.onbeforeunload = () => {
                    if (io) {
                        this.setConnectionStatus(false);
                        io.close();
                    }
                };
            },

            /**
             * Set connection status
             * @param {Boolean} data Status
             */
            setConnectionStatus(status) {
                Vue.set(this, "isConnected", status);
            },

            /**
             * Append new log data
             * @param {Object} data Log data
             */
            appendLog(data) {
                this.logs.unshift(data);
            },

            /**
             * Reset filter data
             */
            resetFilter() {
                Vue.set(this, "filter", {
                    tag: "",
                    log: "",
                });
            },
        },
    }).$mount("#app");
};

Monitor.init();
