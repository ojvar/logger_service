"use strict";

import _ from "lodash";
import Vue from "vue";

window._ = _;
window.Vue = Vue;
