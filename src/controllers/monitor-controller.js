"use strict";

/**
 * Monitor controller
 */
function MonitorController() {}
module.exports = MonitorController;

/**
 * Index page
 * @param {Request} req Request object
 * @param {Response} res Response object
 * @param {Function} next Next function
 */
MonitorController.index = async function index(req, res, next) {
    const wsConfig = config("ws");
    const wsServer = `wss://${wsConfig.host}:${wsConfig.port}`;

    res.render("monitor.pug", { req, wsServer });
};
