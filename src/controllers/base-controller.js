"use strict";

/**
 * BaseController
 */
function BaseController() {}
module.exports = BaseController;

/**
 * Send status-200 result
 * @param {Response} res Response object
 * @param {Object} data Data to send
 */
BaseController.suceess = function suceess(res, data) {
    res.status(200).send(data).end();
};

/**
 * Send status-404 result
 * @param {Response} res Response object
 * @param {Object} data Data to send
 */
BaseController.fail = function fail(res, data) {
    res.status(404).send(data).end();
};
