"use strict";

const BaseController = use("src/controllers/base-controller");

/**
 * Logger Controller
 */
function LoggerController() {}
module.exports = LoggerController;

/**
 * Log data
 * @param {String} tag Tag data
 * @param {String} log Log data
 */
LoggerController.log = async function log(type, tag, log) {
    const Log = db.model("Log");

    await Log.create({
        type,
        tag,
        log,
    });

    /* Send to data */
    const data = { type, tag, log, created_at: new Date() };
    ws.send(data, tag);
    ws.send(data);

    return data;
};

/**
 * Info method
 * @param {Request} req Request object
 * @param {Response} res Response object
 * @param {Function} next Next function
 */
LoggerController.info = async function info(req, res, next) {
    const { tag, log } = req.body;
    const data = await LoggerController.log("info", tag, log);

    BaseController.suceess(res, data);
};

/**
 * Warning method
 * @param {Request} req Request object
 * @param {Response} res Response object
 * @param {Function} next Next function
 */
LoggerController.warning = async function warning(req, res, next) {
    const { tag, log } = req.body;
    const data = await LoggerController.log("warning", tag, log);

    BaseController.suceess(res, data);
};

/**
 * Error method
 * @param {Request} req Request object
 * @param {Response} res Response object
 * @param {Function} next Next function
 */
LoggerController.error = async function error(req, res, next) {
    const { tag, log } = req.body;
    const data = await LoggerController.log("error", tag, log);

    BaseController.suceess(res, data);
};

/**
 * Verbose method
 * @param {Request} req Request object
 * @param {Response} res Response object
 * @param {Function} next Next function
 */
LoggerController.verbose = async function verbose(req, res, next) {
    const { tag, log } = req.body;
    const data = await LoggerController.log("verbose", tag, log);

    BaseController.suceess(res, data);
};
