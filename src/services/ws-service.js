"use strict";

const FS = require("fs");
const Https = require("https");
const SocketIO = require("socket.io");

/**
 * WebSocker service
 */
function WsService() {}
module.exports = WsService;

/**
 * Init Server
 */
WsService.init = async function init() {
    const { port, host, perMessageDeflate } = config("ws");
    const certPath = rPath("src/certificates/ws_server.cert");
    const keyPath = rPath("src/certificates/ws_server.key");

    const server = Https.createServer({
        cert: FS.readFileSync(certPath, "utf-8"),
        key: FS.readFileSync(keyPath, "utf-8"),
    });
    const io = new SocketIO(server);

    /* Bind events */
    WsService.bindEvents(io, server);

    /* Start listening */
    server.listen(port, host, () => {
        console.log(`
SocketIO server started
        Url:    wss://${host}:${port}
        Host:    ${host}
        Port:    ${port}
`);
    });

    /* Keep local data */
    WsService.io = io;
    WsService.server = server;

    /* Setup-up global variable */
    global.ws = WsService;
};

/**
 * Bind events
 * @param {WebSocker} io The web-socket instance object
 */
WsService.bindEvents = function bindEvents(io, server) {
    io.on("connection", (client) => {
        console.log("Client connected");

        client.on("event", (data) => {
            console.log("received: %s", data);
        });

        client.on("disconnect", () => {
            console.log("Client disconnected %s");
        });
    });

    // server.on("upgrade", async (request, socket, head) => {
    //     console.log("Connection Upgrade...");

    //     let client = null;

    //     /* TODO: AUTHENTICATION COMES HERE */
    //     /*
    //     let result = await authenticate(request);

    //     if (result) {
    //         socket.write("HTTP/1.1 401 Unauthorized\r\n\r\n");
    //         socket.destroy();
    //         return;
    //     }
    //     */

    //     /* TODO: FACED WITH ERROR */
    //     // wss.handleUpgrade(request, socket, head, (ws) => {
    //     //     wss.emit("connection", ws, request, client);
    //     // });
    // });
};

/**
 * Send data
 */
WsService.send = async function send(data, channel) {
    if (!WsService.io) {
        return;
    }

    /* Convert to string data */
    if (typeof data !== "string") {
        data = JSON.stringify(data);
    }

    if (channel) {
        WsService.io.emit(channel, data);
    } else {
        WsService.io.send(data);
    }
};
